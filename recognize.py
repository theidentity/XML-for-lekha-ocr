# -*- coding: utf-8 -*-
import cv2
import preprocess as pp
import training as train
label_uni=train.label_uni

def recognize_letter(feature):
	a = train.classifier.predict(feature)
	if (label_uni[int(a)]=='0' or label_uni[int(a)]=='ഠ'):
		if(pp.previous_char==None):
			return label_uni.index('0')
		if(label_uni[int(pp.previous_char.label)].isdigit()):
			return label_uni.index('0')
		if(pp.cur_char.height<=(pp.previous_char.height*3/4)):
			return label_uni.index('ം')
		return label_uni.index('ഠ')
	return a

def recognize(url):
	# print (url)
	img=cv2.imread(url,0)
	if(img.data==None):
		print url+' does\'nt exist'
		exit()
	img = pp.preprocess(img)
	im=img
	im,rot = pp.skew_correction(img)
	# Add Layout analysis here
	print recognize_block(im)
'''Recgnizing a block of scanned image'''
def recognize_block(im):
	line = pp.find_lines(im)
	# print len(linene)
	label_list=label_uni
	i=0
	string=''
	#selecting each line
	for l in line:
		# cv2.imwrite('temp/zline_temp_'+str(i)+'.png',l.data)
		string=string+'\n'
		j=0
		#selecting words in a line
		for w in l.word_list:
			#cv2.imwrite('zword_temp_'+str(i)+'_word_'+str(j)+'.png',w.data)
			string=string+' '
			j+=1
			k=0
			c=0

			#Formatting characters in the word
			while(c<len(w.char_list)):
				char= w.char_list[c]
				try:
					#checking whether the input is  ' or " or ,
					if(label_list[int(char.label)]in ['\'',',']):
						char2=w.char_list[c+1]
						if(label_list[int(char2.label)]in ['\'',',']):
							string=string+'\"'
							c+=1
						else:
							string=string+label_list[int(char.label)]
					#checking whether the input is  ൈ  or െ
					elif(label_list[int(char.label)]in ['െ','േ','്ര']):
						char2=w.char_list[c+1]
						if(label_list[int(char2.label)]in ['െ','്ര']):
							char3=w.char_list[c+2]
							string=string+label_list[int(char3.label)]
							c+=1
						string=string+label_list[int(char2.label)]
						string=string+label_list[int(char.label)]
						c+=1
					else:
						string=string+label_list[int(char.label)]
				except IndexError:
					string=string+label_list[int(char.label)]
				# cv2.imwrite('output/zcline_'+str(i)+'_temp_word_'+str(j)+'_c_'+str(k)+str(int(w.char_list[c].label))+'.png',w.char_list[c].data)
				k+=1
				c+=1
		i+=1
	return string
