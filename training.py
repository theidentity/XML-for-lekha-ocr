# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 10:26:15 2015

@author: james
"""
#feature extraction and Recognition
from __future__ import division
import cv2
import numpy as np
import os
import shutil
import itertools
import glob
from random import shuffle

from path import *
import preprocess as pp
import features

label_uni = []
f = open(PATH_LABEL_FILE,'r')
for l in f:
	label_uni.append(l[:-1])
def label_unicode():
	return label_uni
# label_unicode()
label_uni.append('ം')

def load():
	classifier.load('svm_class.xml')
# load()
classifier = cv2.SVM()
classifier.load('svm_class.xml')
#include


def train_svm():
	svm_params = dict( kernel_type = cv2.SVM_RBF,
	                    svm_type = cv2.SVM_C_SVC,
	                    C=9.34, gamma=15.68 )
	svm=cv2.SVM()
	label_list=[]
	label_list.append('a')
	url='train_images/'
	s_list=sorted(os.listdir(url))
	train_set = []
	label = 0
	# print (s_list)
	for i in s_list:
		s_list=glob.glob(url+i+'/*.png')
		if(len(s_list)>25):
			file=open(url+i+'/utf8',"r")
			i_uni=file.read()
			i_uni=i_uni[:-1]
			label_list.append(i_uni)
			label+=1
		else:
			continue
		print str(label),i,label_list[label],len(s_list)
		for j in s_list:
			img=cv2.imread(j,0)
			img=preprocess(img)
			f =features.find_feature(img.copy())
			# print len(f)
			s = [label,f]
			train_set.append(s)
	f=open('label','w')
	for l in label_list:
		f.write(l+'\n')
	f.close()

	shuffle(train_set)
	f_list = []
	label = []
	for t in train_set:
		label.append(t[0])
		f_list.append(t[1])
	# np.savetxt('feature.txt',f_list)
	# np.savetxt('label.txt',label)
	# samples = np.loadtxt('feature.txt',np.float32)
	# responses = np.loadtxt('label.txt',np.float32)33
	# responses = responses.reshape((responses.size,1))
	samples = np.array(f_list,np.float32)
	responses = np.array(label,np.float32)
	print 'auto training initiated'
	print 'please wait.....'
	svm.train(samples,responses,params=svm_params)
	# svm.train_auto(samples,responses,None,None,params=svm_params)
	svm.save("svm_class.xml")
def preprocess(img):#eliptical kernel
	"""Does adaptive thresholding to the image.Converts it into a binary image"""
	# cv2.imwrite('before_temp_thresholding.png',img)
	img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,243,50)
	# cv2.imwrite('after_temp_thresholding.png',img)
	return img
# train_svm();
